<?php

namespace Disasterdrop\RestApiClient\Api\ResourceHandler;

use Disasterdrop\RestApiClient\Api\Entity\EntityInterface;

class ResourceHandler {
  
  protected $client;
  
  protected $entityObject;
  
  protected $url;
  
  protected $queryParams;
  
  protected $resource;
  
  protected $links;
  
  protected $embedded;
  
  public function __construct(Client $client) {
    $this->client = $client;
  }
  
  /**
   *
   */
  public function request($url) {
    if(!is_string($url)) {
      throw new \InvalidArgumentException("URL must be string");
    }
    
    $this->url = $url;
    
    return $this;
  }
  
  /**
   *
   */
  public function willReturn(EntityInterface $className) {
    return $this;  
  }
  
  public function fetch() {
    
  }
  
  public function save() {}
  
  public function delete() {}
  
  public function update() {}
  
}